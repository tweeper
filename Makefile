# Packagers may want to override this!
prefix ?= /usr/local

PHP_SCRIPT_DIR ?= $(prefix)/share/php
BIN_DIR := $(prefix)/bin
MAN_DIR := $(prefix)/share/man

TWEEPER_DIR := $(PHP_SCRIPT_DIR)/tweeper

all:

clean:
	rm -f tweeper.1

docs:
	a2x -f manpage tweeper.1.asciidoc

installdocs: docs
	install -d $(DESTDIR)$(MAN_DIR)/man1
	install -m644 tweeper.1 $(DESTDIR)$(MAN_DIR)/man1

install: installdocs
	install -d $(DESTDIR)$(TWEEPER_DIR)
	install -m644 *.php $(DESTDIR)$(TWEEPER_DIR)
	install -m755 tweeper $(DESTDIR)$(TWEEPER_DIR)
	install -d $(DESTDIR)$(TWEEPER_DIR)/src
	install -m644 src/* $(DESTDIR)$(TWEEPER_DIR)/src
	install -d $(DESTDIR)$(BIN_DIR)
	ln -rsf $(DESTDIR)$(TWEEPER_DIR)/tweeper $(DESTDIR)$(BIN_DIR)/tweeper
	@echo -e "\n\nINSTALLATION COMPLETE"
	@echo -e "Make sure '$(DESTDIR)$(PHP_SCRIPT_DIR)' is in PHP include_path!\n"
