<?php

/**
 * @file
 * Tweeper - a Twitter to RSS web scraper.
 *
 * Copyright (C) 2013-2020  Antonio Ospite <ao2@ao2.it>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

require_once 'autoload.php';

use Tweeper\Tweeper;

date_default_timezone_set('UTC');

/**
 * Check if the script is being run from the command line.
 */
function is_cli() {
  return (php_sapi_name() === "cli");
}

/**
 * Show the script usage.
 */
function usage($argv) {
  if (is_cli()) {
    $usage = "{$argv[0]} [-e|-m <0|1>|-u <0|1>|-v <0|1>|-h|--help] <src_url>\n";
  }
  else {
    $usage = htmlentities("{$_SERVER['SCRIPT_NAME']}?src_url=<src_url>&generate_enclosure=<0|1>&show_usernames=<0|1>&show_multimedia=<0|1>&verbose_output=<0|1>");
  }

  return "usage: $usage";
}

/**
 * Parse command line options.
 */
function parse_options_cli($argv, $argc) {
  $options = [
    'generate_enclosure' => FALSE,
    'show_usernames' => TRUE,
    'show_multimedia' => TRUE,
    'verbose_output' => TRUE,
  ];

  if ($argc < 2) {
    return $options;
  }

  $cli_options = getopt("em:u:v:h", ["help"]);
  foreach ($cli_options as $opt => $val) {
    switch ($opt) {
      case 'e':
        $options['generate_enclosure'] = TRUE;
        break;

      case 'm':
        $ret = filter_var($val, FILTER_VALIDATE_BOOLEAN, FILTER_NULL_ON_FAILURE);
        if (NULL === $ret) {
          fwrite(STDERR, "Invalid argument for the -m option.\n");
          fwrite(STDERR, usage($argv));
          exit(1);
        }
        $options['show_multimedia'] = $val;
        break;

      case 'u':
        $ret = filter_var($val, FILTER_VALIDATE_BOOLEAN, FILTER_NULL_ON_FAILURE);
        if (NULL === $ret) {
          fwrite(STDERR, "Invalid argument for the -u option.\n");
          fwrite(STDERR, usage($argv));
          exit(1);
        }
        $options['show_usernames'] = $val;
        break;

      case 'v':
        $ret = filter_var($val, FILTER_VALIDATE_BOOLEAN, FILTER_NULL_ON_FAILURE);
        if (NULL === $ret) {
          fwrite(STDERR, "Invalid argument for the -v option.\n");
          fwrite(STDERR, usage($argv));
          exit(1);
        }
        $options['verbose_output'] = $val;
        break;

      case 'h':
      case 'help':
        echo usage($argv);
        exit(0);

      default:
        fwrite(STDERR, usage($argv));
        exit(1);
    }
  }

  // For now assume that the URL is the lest argument, in the future we could
  // switch to PHP >= 7.1 and use the $optind argument of getopt().
  $options['src_url'] = array_pop($argv);

  return $options;
}

/**
 * Parse options passed from a query string.
 */
function parse_options_query_string() {
  $options = [
    'generate_enclosure' => FALSE,
    'show_usernames' => TRUE,
    'show_multimedia' => TRUE,
    'verbose_output' => TRUE,
  ];

  if (isset($_GET['src_url'])) {
    $options['src_url'] = $_GET['src_url'];
  }

  if (isset($_GET['generate_enclosure'])) {
    $options['generate_enclosure'] = $_GET['generate_enclosure'] == 1;
  }

  if (isset($_GET['show_multimedia'])) {
    $options['show_multimedia'] = $_GET['show_multimedia'] != 0;
  }

  if (isset($_GET['show_usernames'])) {
    $options['show_usernames'] = $_GET['show_usernames'] != 0;
  }

  if (isset($_GET['verbose_output'])) {
    $options['verbose_output'] = $_GET['verbose_output'] != 0;
  }

  return $options;
}

if (is_cli()) {
  $options = parse_options_cli($argv, $argc);
  $error_stream = fopen('php://stderr', 'w');
}
else {
  $options = parse_options_query_string();
  $error_stream = fopen('php://output', 'w');
}

if (!isset($options['src_url'])) {
  fwrite($error_stream, usage(is_cli() ? $argv : NULL));
  exit(1);
}

$tweeper = new Tweeper($options['generate_enclosure'], $options['show_usernames'], $options['show_multimedia'], $options['verbose_output']);
$output = $tweeper->tweep($options['src_url']);
if (is_null($output)) {
  exit(1);
}
echo $output;
