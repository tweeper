#!/bin/sh
#
# Facebook requires a CAPTCHA most of the times, so keep fetching the URL as
# long as needed, until the page is shown with no CAPTCHA.

set -e

USER_AGENT="Mozilla/5.0 (Windows NT 6.1; rv:22.0) Gecko/20130405 Firefox/22.0";

while true;
do
  # Force language to en-us to make sure that the string matching works
  OUTPUT=$(wget -nv --user-agent="$USER_AGENT" --header='Accept-Language: en-us' -O - -- "$1")
  if echo $OUTPUT | grep -q -v "Security Check Required";
  then
    echo "$OUTPUT" > facebook.html
    break
  fi
  sleep 5
done
