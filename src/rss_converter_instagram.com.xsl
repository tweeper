<!--
  Stylesheet to convert Instagram user timelines to RSS.

  Copyright (C) 2015-2018  Antonio Ospite <ao2@ao2.it>

  This file is part of tweeper.

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
-->
<xsl:stylesheet version="1.0"
    xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:php="http://php.net/xsl"
    exclude-result-prefixes="php">

    <xsl:param name="generate-enclosure"/>
    <xsl:param name="show-usernames"/>
    <xsl:param name="show-multimedia"/>

    <xsl:output method="xml" indent="yes"/>

    <xsl:variable name="BaseURL">
        <xsl:text>https://instagram.com</xsl:text>
    </xsl:variable>

    <xsl:variable name="user-name" select="//ProfilePage/graphql/user/username"/>

    <!--
         NOTE: some users do not specify the full name.

         Remember to handle this case when using it and fall-back to the plain
         user name when appropriate.
    -->
    <xsl:variable name="full-name" select="//ProfilePage/graphql/user/full_name"/>

    <xsl:variable name="location-name" select="//LocationsPage/graphql/location/name"/>

    <xsl:variable name="hashtag-name" select="//TagPage/graphql/hashtag/name"/>

    <xsl:variable name="screen-name">
        <xsl:choose>
            <xsl:when test="$location-name != ''">
                <xsl:value-of select="$location-name"/>
            </xsl:when>
            <xsl:when test="$hashtag-name != ''">
                <xsl:value-of select="$hashtag-name"/>
            </xsl:when>
            <xsl:when test="$full-name != ''">
                <xsl:value-of select="$full-name"/>
            </xsl:when>
            <xsl:otherwise>
                <xsl:value-of select="$user-name"/>
            </xsl:otherwise>
        </xsl:choose>
    </xsl:variable>

    <xsl:template match="//edges/node">
        <xsl:variable name="item-content-image" select="./display_url"/>
        <xsl:variable name="item-content-caption" select="./edge_media_to_caption/edges/node/text"/>
        <xsl:variable name="item-permalink" select="concat($BaseURL, '/p/', ./shortcode, '/')"/>
        <item>
            <title>
                <xsl:variable name="title-length" select="140"/>
                <xsl:variable name="item-content-title">
                    <xsl:if test="$show-usernames = 1">
                        <xsl:value-of select="concat($screen-name, ': ')"/>
                    </xsl:if>
                    <xsl:value-of select="normalize-space($item-content-caption)"/>
                </xsl:variable>
                <!-- ellipsize, inspired from http://stackoverflow.com/questions/13622338 -->
                <xsl:choose>
                    <xsl:when test="string-length($item-content-title) > $title-length">
                        <xsl:variable name="truncated-length" select="$title-length - 3"/>
                        <xsl:value-of select="substring($item-content-title, 1, $truncated-length)"/>
                        <xsl:text>...</xsl:text>
                    </xsl:when>
                    <xsl:otherwise>
                        <xsl:value-of select="$item-content-title"/>
                    </xsl:otherwise>
                </xsl:choose>
            </title>
            <link>
                <xsl:value-of select="$item-permalink"/>
            </link>
            <guid>
                <xsl:value-of select="$item-permalink"/>
            </guid>
            <pubDate>
                <xsl:variable name="timestamp" select="./taken_at_timestamp"/>
                <xsl:value-of select="php:functionString('Tweeper\Tweeper::epochToRssDate', $timestamp)"/>
            </pubDate>
            <description>
                <xsl:text disable-output-escaping="yes">&lt;![CDATA[</xsl:text>
                <p>
                    <xsl:if test="./is_video/text() = 1">
                        (Video)
                    </xsl:if>
                    <xsl:value-of select="$item-content-caption"/>
                </p><br />
                <xsl:if test="$show-multimedia = 1">
                    <a href="{$item-permalink}"><img src="{$item-content-image}" style="max-width: 100%"/></a>
                </xsl:if>
                <xsl:text disable-output-escaping="yes">]]&gt;</xsl:text>
            </description>
            <xsl:if test="$generate-enclosure = 1">
                <xsl:copy-of select="php:functionString('Tweeper\Tweeper::generateEnclosure', $item-content-image)"/>
            </xsl:if>
        </item>
    </xsl:template>

    <xsl:template match="/">

        <xsl:variable name="channel-title" select="concat('Instagram / ', $screen-name)"/>
        <xsl:variable name="channel-link">
            <xsl:choose>
                <xsl:when test="$location-name != ''">
                    <xsl:variable name="location-id" select="//LocationsPage/graphql/location/id"/>
                    <xsl:value-of select="concat($BaseURL, '/explore/locations/', $location-id)"/>
                </xsl:when>
                <xsl:when test="$hashtag-name != ''">
                    <xsl:value-of select="concat($BaseURL, '/explore/tags/', $hashtag-name)"/>
                </xsl:when>
                <xsl:otherwise>
                    <xsl:value-of select="concat($BaseURL, '/', $user-name)"/>
                </xsl:otherwise>
            </xsl:choose>
        </xsl:variable>
        <xsl:variable name="channel-image" select="//profile_pic_url"/>

        <rss version="2.0">
            <xsl:attribute name="xml:base"><xsl:value-of select="$BaseURL" /></xsl:attribute>
            <channel>
                <generator>Tweeper</generator>
                <title>
                    <xsl:value-of select="$channel-title"/>
                </title>
                <link>
                    <xsl:value-of select="$channel-link"/>
                </link>
                <description>
                    <xsl:text disable-output-escaping="yes">&lt;![CDATA[</xsl:text>
                    <xsl:choose>
                        <xsl:when test="$location-name != ''">
                            <xsl:variable name="location-latitude" select="//LocationsPage/graphql/location/lat"/>
                            <xsl:variable name="location-longitude" select="//LocationsPage/graphql/location/lng"/>
                            <xsl:value-of select="concat($location-name, ' (', $location-latitude, ', ', $location-longitude, ')')"/>
                        </xsl:when>
                        <xsl:when test="$hashtag-name != ''">
                            <xsl:value-of select="concat('#', $hashtag-name)"/>
                        </xsl:when>
                        <xsl:otherwise>
                            <xsl:value-of select="normalize-space(concat($screen-name, '. ', //user/biography))"/>
                            <xsl:variable name="external-url" select="//user/external_url"/>
                            <xsl:if test="$external-url != ''">
                                <xsl:text> </xsl:text><a href="{$external-url}"><xsl:value-of select="$external-url"/></a>
                            </xsl:if>
                        </xsl:otherwise>
                    </xsl:choose>
                    <xsl:text disable-output-escaping="yes">]]&gt;</xsl:text>
                </description>
                <xsl:if test="$channel-image != ''">
                    <image>
                        <title>
                            <xsl:value-of select="$channel-title"/>
                        </title>
                        <link>
                            <xsl:value-of select="$channel-link"/>
                        </link>
                        <url>
                            <xsl:value-of select="$channel-image"/>
                        </url>
                    </image>
                </xsl:if>
                <xsl:apply-templates select="//ProfilePage/graphql/user/edge_owner_to_timeline_media/edges/node|//LocationsPage/graphql/location/edge_location_to_media/edges/node|//TagPage/graphql/hashtag/edge_hashtag_to_media/edges/node"/>
            </channel>
        </rss>
    </xsl:template>
</xsl:stylesheet>
