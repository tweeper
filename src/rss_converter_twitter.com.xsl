<!--
  Stylesheet to convert Twitter user timelines to RSS.

  Copyright (C) 2013-2020  Antonio Ospite <ao2@ao2.it>

  This file is part of tweeper.

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
-->
<xsl:stylesheet version="1.0"
    xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:php="http://php.net/xsl"
    exclude-result-prefixes="php">

    <xsl:param name="generate-enclosure"/>
    <xsl:param name="show-usernames"/>
    <xsl:param name="show-multimedia"/>

    <xsl:output method="xml" indent="yes"/>

    <xsl:variable name="BaseURL">
        <xsl:text>https://twitter.com</xsl:text>
    </xsl:variable>

    <!-- Identity transform -->
    <xsl:template match="@*|node()">
        <xsl:copy>
            <!--
                Strip the style attribute while copying elements because it may be
                dangerous, see:
                https://validator.w3.org/feed/docs/warning/DangerousStyleAttr.html
            -->
            <xsl:apply-templates select="@*[not(name() = 'style')]|node()"/>
        </xsl:copy>
    </xsl:template>

    <!--
         Anchors to external links provide the direct URL in the
         data-expanded-url attribute, so use this in the href attribute too
         instead of the default short URL which uses the t.co redirection
         service.

         NOTE: when creating an element, attributes must be processed _before_
         adding the contents (either children or a value):
         http://stackoverflow.com/questions/21984867/
    -->
    <xsl:template match="a[@data-expanded-url]">
        <!-- Prepend and append a white space for aestethic reasons -->
        <xsl:text> </xsl:text>
        <a>
            <xsl:attribute name="href">
                <xsl:value-of select="@data-expanded-url"/>
            </xsl:attribute>
            <!-- Also strip &nbsp; and &hellip; -->
            <xsl:value-of select="translate(., '&#xA0;&#x2026;', '')"/>
        </a>
        <xsl:text> </xsl:text>
    </xsl:template>

    <!--
         These are links to pic.twitter.com, use the direct link for those
         too instead of the t.co redirections.
    -->
    <xsl:template match="a[@data-pre-embedded='true']">
        <xsl:if test="$show-multimedia = 1">
            <!-- Prepend and append a white space for aestethic reasons -->
            <xsl:text> </xsl:text>
            <a>
                <xsl:attribute name="href">
                    <xsl:value-of select="concat('https://', .)"/>
                </xsl:attribute>
                <xsl:value-of select="concat('https://', .)"/>
            </a>
            <xsl:text> </xsl:text>
        </xsl:if>
    </xsl:template>

    <!-- Present images in a more convenient way -->
    <xsl:template match="div[@data-image-url]">
        <a>
            <xsl:attribute name="href">
                <xsl:value-of select="concat(@data-image-url, ':orig')"/>
            </xsl:attribute>
            <img style="max-width: 100%">
                <xsl:attribute name="src">
                    <xsl:value-of select="@data-image-url"/>
                </xsl:attribute>
            </img>
        </a>
    </xsl:template>

    <!-- Don't repeat background in embedded media content -->
    <xsl:template match="div[contains(@class, 'PlayableMedia-player')]">
        <xsl:copy>
            <xsl:apply-templates select="@*"/>
            <xsl:attribute name="style">
                <xsl:value-of select="concat(@style, '; background-repeat: no-repeat; background-size: 100% auto')"/>
            </xsl:attribute>
            <xsl:apply-templates select="node()"/>
        </xsl:copy>
    </xsl:template>

    <xsl:template match="a[@data-expanded-url]" mode="enclosure">
        <xsl:copy-of select="php:functionString('Tweeper\Tweeper::generateEnclosure', ./@data-expanded-url)"/>
    </xsl:template>

    <xsl:template match="div[@data-image-url]" mode="enclosure">
        <xsl:copy-of select="php:functionString('Tweeper\Tweeper::generateEnclosure', concat(./@data-image-url, ':orig'))"/>
    </xsl:template>

    <xsl:variable name="screen-name" select="//div[@class='user-actions btn-group not-following ']/@data-screen-name"/>

    <xsl:template match="//div[@class='permalink-inner permalink-tweet-container'] | //li[@data-item-id and @data-item-type='tweet']">
        <xsl:variable name="user-name" select=".//div[@data-tweet-id]/@data-screen-name"/>
        <xsl:variable name="item-content" select=".//p[contains(@class, 'js-tweet-text')]"/>
        <xsl:variable name="item-media" select=".//div[contains(@class, 'AdaptiveMedia-container')]"/>
        <xsl:variable name="item-permalink" select="concat($BaseURL, .//div[@data-permalink-path]/@data-permalink-path)"/>

        <xsl:variable name="item-has-video" select="$item-media//*[contains(@class, 'PlayableMedia--video')]"/>
        <xsl:variable name="item-has-gif" select="$item-media//*[contains(@class, 'PlayableMedia--gif')]"/>
        <item>
            <title>
                <xsl:if test="($show-usernames = 1) or ($screen-name != $user-name)">
                    <xsl:value-of select="concat($user-name, ': ')"/>
                </xsl:if>
                <xsl:if test="$item-has-video">
                    <xsl:text>(Video) </xsl:text>
                </xsl:if>
                <!--
                     Prepend a space in front of the URLs which are not
                     preceded by an open parenthesis, for aestethic reasons.
                     Also, regex, I know: http://xkcd.com/1171/
                -->
                <xsl:variable
                    name="processed-title"
                    select="php:functionString('preg_replace', '@((?&lt;!\()(?:http[s]?://|pic.twitter.com))@', ' \1', $item-content)"/>
                <!-- Also strip &nbsp; and &hellip; -->
                <xsl:value-of select="normalize-space(translate($processed-title, '&#xA0;&#x2026;', ''))"/>
            </title>
            <link>
                <xsl:value-of select="$item-permalink"/>
            </link>
            <guid>
                <xsl:value-of select="$item-permalink"/>
            </guid>
            <pubDate>
                <xsl:variable name="timestamp" select=".//span[contains(@class, 'js-short-timestamp')]/@data-time"/>
                <xsl:value-of select="php:functionString('Tweeper\Tweeper::epochToRssDate', number($timestamp))"/>
            </pubDate>
            <description>
                <xsl:text disable-output-escaping="yes">&lt;![CDATA[</xsl:text>
                <xsl:if test="($show-usernames = 1) or ($screen-name != $user-name)">
                    <xsl:value-of select="concat($user-name, ':')"/>
                    <xsl:element name="br"/>
                </xsl:if>
                <xsl:if test="$item-has-video">
                    <xsl:text> (Video)</xsl:text>
                    <xsl:element name="br"/>
                </xsl:if>
                <xsl:if test="$item-has-gif">
                    <xsl:text> (GIF)</xsl:text>
                    <xsl:element name="br"/>
                </xsl:if>
                <xsl:element name="span">
                    <xsl:attribute name="style">white-space: pre-wrap;</xsl:attribute>
                    <xsl:apply-templates select="$item-content/node()"/>
                </xsl:element>
                <xsl:if test="$show-multimedia = 1">
                    <xsl:apply-templates select="$item-media/node()"/>
                </xsl:if>
                <xsl:text disable-output-escaping="yes">]]&gt;</xsl:text>
            </description>
            <xsl:if test="$generate-enclosure = 1">
                <xsl:apply-templates select="$item-content//a[@data-expanded-url]" mode="enclosure"/>
                <xsl:apply-templates select="$item-media//div[@data-image-url]" mode="enclosure"/>
            </xsl:if>
        </item>
    </xsl:template>

    <xsl:template match="/">
        <xsl:variable name="channel-title">
            <xsl:choose>
                <xsl:when test="$screen-name != ''">
                    <xsl:value-of select="concat('Twitter / ', $screen-name)"/>
                </xsl:when>
                <xsl:otherwise>
                    <xsl:value-of select="concat('Twitter / ', normalize-space(//h1[1]))"/>
                </xsl:otherwise>
            </xsl:choose>
        </xsl:variable>
        <xsl:variable name="channel-link" select="//link[@rel='canonical']/@href"/>
        <xsl:variable name="channel-image" select="//a[contains(@class, 'profile-picture')]/@href"/>

        <rss version="2.0">
            <xsl:attribute name="xml:base"><xsl:value-of select="$BaseURL" /></xsl:attribute>
            <channel>
                <generator>Tweeper</generator>
                <title>
                    <xsl:value-of select="$channel-title"/>
                </title>
                <link>
                    <xsl:value-of select="$channel-link"/>
                </link>
                <description>
                    <xsl:value-of select="normalize-space(//div[@class='ProfileHeaderCard'])"/>
                    <!-- The following rule should only match on hashtag URLs -->
                    <xsl:value-of select="normalize-space(//div[@class='SearchNavigation-textContainer'])"/>
                </description>
                <xsl:if test="$channel-image != ''">
                    <image>
                        <title>
                            <xsl:value-of select="$channel-title"/>
                        </title>
                        <link>
                            <xsl:value-of select="$channel-link"/>
                        </link>
                        <url>
                            <xsl:value-of select="$channel-image"/>
                        </url>
                    </image>
                </xsl:if>
                <xsl:apply-templates select="//ol[@id='stream-items-id']/li[@data-item-id and @data-item-type='tweet' and not(contains(@class, 'has-profile-promoted-tweet'))]"/>

                <!-- These rules will only match on permalink URLs -->
                <xsl:apply-templates select="//div[@class='permalink-inner permalink-tweet-container']"/>
                <xsl:apply-templates select="//div[@data-component-context='replies']//li[@data-item-id and @data-item-type='tweet' and not(contains(@class, 'has-profile-promoted-tweet'))]"/>

            </channel>
        </rss>
    </xsl:template>
</xsl:stylesheet>
