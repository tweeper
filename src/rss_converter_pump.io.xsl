<!--
  Stylesheet to convert Pump.io activity streams to RSS.

  Copyright (C) 2013-2018  Antonio Ospite <ao2@ao2.it>

  This file is part of tweeper.

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
-->
<!-- To Evan, please reconsider publishing RSS ouput for _public_ contents -->
<xsl:stylesheet version="1.0"
    xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:php="http://php.net/xsl"
    exclude-result-prefixes="php">

    <xsl:param name="generate-enclosure"/>
    <xsl:param name="show-usernames"/>

    <xsl:output method="xml" indent="yes"/>

    <xsl:variable name="domain-name" select="substring-after(//div[@id='profile-block']/@data-profile-id, '@')"/>
    <xsl:variable name="BaseURL" select="concat('https://', $domain-name)"/>

    <xsl:variable name="user-name" select="substring-after(//div[@id='profile-block']/@data-profile-id, ':')"/>

    <xsl:template match="//div[@id='user-content-activities']//ul[@id='major-stream']/li">
        <xsl:variable name="item-content" select=".//div[@class='activity-content']"/>
        <xsl:variable name="item-permalink" select=".//p[@class='muted']/small/a/@href"/>
        <item>
            <title>
                <xsl:if test="$show-usernames = 1">
                    <xsl:value-of select="concat($user-name, ': ')"/>
                </xsl:if>
                <xsl:value-of select="normalize-space($item-content)"/>
            </title>
            <link>
                <xsl:value-of select="$item-permalink"/>
            </link>
            <guid>
                <xsl:value-of select="$item-permalink"/>
            </guid>
            <pubDate>
                <xsl:value-of select="php:functionString('Tweeper\Tweeper::strToRssDate', .//abbr[@class='easydate']/@title)"/>
            </pubDate>
            <description>
                <xsl:text disable-output-escaping="yes">&lt;![CDATA[</xsl:text>
                <xsl:if test="$show-usernames = 1">
                    <xsl:value-of select="concat($user-name, ': ')"/>
                </xsl:if>
                <xsl:copy-of select="$item-content/node()"/>
                <xsl:text disable-output-escaping="yes">]]&gt;</xsl:text>
            </description>
            <xsl:if test="$generate-enclosure = 1">
                <xsl:variable name="image-thumb-link" select=".//img[contains(@class, 'object-image')]/@src"/>
                <xsl:if test="$image-thumb-link">
                    <xsl:variable name="image-link" select="php:functionString('str_replace', '_thumb', '', $image-thumb-link)"/>
                    <xsl:copy-of select="php:functionString('Tweeper\Tweeper::generateEnclosure', $image-link)"/>
                </xsl:if>
            </xsl:if>
        </item>
    </xsl:template>

    <xsl:template match="/">
        <xsl:variable name="channel-title" select="concat(substring-after($user-name, '@'), ' / ', substring-before($user-name, '@'))"/>
        <xsl:variable name="channel-link" select="concat('https://', substring-after($user-name, '@'), '/', substring-before($user-name, '@'))"/>

        <rss version="2.0">
            <xsl:attribute name="xml:base"><xsl:value-of select="$BaseURL" /></xsl:attribute>
            <channel>
                <generator>Tweeper</generator>
                <title>
                    <xsl:value-of select="$channel-title"/>
                </title>
                <link>
                    <xsl:value-of select="$channel-link"/>
                </link>
                <description>
                    <xsl:value-of select="normalize-space(//h1[@class='media-header'])"/>
                </description>
                <image>
                    <title>
                        <xsl:value-of select="$channel-title"/>
                    </title>
                    <link>
                        <xsl:value-of select="$channel-link"/>
                    </link>
                    <url>
                        <xsl:value-of select="//div[@id='profile-block']/span/img[contains(@class, 'img-rounded media-object')]/@src"/>
                    </url>
                </image>
                <xsl:apply-templates select="//div[@id='user-content-activities']//ul[@id='major-stream']/li"/>
            </channel>
        </rss>
    </xsl:template>
</xsl:stylesheet>
