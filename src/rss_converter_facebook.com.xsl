<!--
  Stylesheet to convert a Facebook public page to RSS.

  Copyright (C) 2015-2018  Antonio Ospite <ao2@ao2.it>

  This file is part of tweeper.

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
-->

<!--
  Since June 23rd, 2015 facebook.com deprecated the RSS feed endpoint for public pages:
  https://developers.facebook.com/docs/apps/changelog#v2_3_90_day_deprecations

  They suggest to use the Graph API but they fail to mention that it does not
  work anymore without authentication, so it cannot be considered an
  _equivalent_ solution.

  Luckily we've got Tweeper!
-->

<xsl:stylesheet version="1.0"
    xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:php="http://php.net/xsl"
    exclude-result-prefixes="php">

    <xsl:output method="xml" indent="yes"/>

    <xsl:variable name="BaseURL">
        <xsl:text>https://facebook.com</xsl:text>
    </xsl:variable>

    <!--
         Extract the page id from an element like:
        <meta property="al:android:url" content="fb://page/793837197390834">

        The page id will be used to build the permalink.
    -->
    <xsl:variable
        name="page-id"
        select="substring-after(//meta[@property='al:android:url']/@content, 'fb://page/')"/>

    <xsl:template match="//div[contains(@class, 'fbUserStory') or contains(@class, 'userContentWrapper')]">
        <xsl:variable name="story-id" select=".//input[@name='ft_ent_identifier']/@value"/>
        <xsl:variable
            name="item-permalink"
            select="concat($BaseURL, '/permalink.php?id=', $page-id, '&amp;story_fbid=', $story-id)"/>

        <!-- Get only the first child in order to skip the footer of the content -->
        <xsl:variable name="item-content" select="div[1]"/>

        <item>
            <title>
                <xsl:variable name="item-title" select="$item-content//p"/>
                <xsl:variable name="title-length" select="140"/>
                <!-- ellipsize, inspired from http://stackoverflow.com/questions/13622338 -->
                <xsl:choose>
                    <xsl:when test="string-length($item-title) > $title-length">
                        <xsl:variable name="truncated-length" select="$title-length - 3"/>
                        <xsl:value-of select="substring($item-title, 1, $truncated-length)"/>
                        <xsl:text>...</xsl:text>
                    </xsl:when>
                    <xsl:otherwise>
                        <xsl:value-of select="$item-title"/>
                    </xsl:otherwise>
                </xsl:choose>
            </title>
            <link>
                <xsl:value-of select="$item-permalink"/>
            </link>
            <guid>
                <xsl:value-of select="$item-permalink"/>
            </guid>
            <pubDate>
                <xsl:variable name="timestamp" select=".//abbr[@data-shorten]/@data-utime"/>
                <xsl:value-of select="php:functionString('Tweeper\Tweeper::epochToRssDate', $timestamp)"/>
            </pubDate>
            <description>

                <!--
                     Get only the children starting from the one with class="userContent",
                     this way the content header is skipped
                -->
                <xsl:variable
                    name="usercontent-position"
                    select="count($item-content/div[contains(@class, 'userContent')]/preceding-sibling::*) + 1"/>

                <xsl:text disable-output-escaping="yes">&lt;![CDATA[</xsl:text>
                <xsl:copy-of select="$item-content/div[position() >= $usercontent-position]"/>
                <xsl:text disable-output-escaping="yes">]]&gt;</xsl:text>
            </description>
        </item>
    </xsl:template>

    <xsl:template match="/">
        <xsl:variable name="channel-title" select="//title"/>
        <xsl:variable name="channel-link" select="//meta[@property='og:url']/@content"/>
        <xsl:variable name="channel-image" select="//meta[@property='og:image']/@content"/>

        <rss version="2.0">
            <xsl:attribute name="xml:base"><xsl:value-of select="$BaseURL" /></xsl:attribute>
            <channel>
                <generator>Tweeper</generator>
                <title>
                    <xsl:value-of select="$channel-title"/>
                </title>
                <link>
                    <xsl:value-of select="$channel-link"/>
                </link>
                <description>
                    <xsl:text disable-output-escaping="yes">&lt;![CDATA[</xsl:text>
                    <xsl:value-of select="//meta[@property='og:description']/@content"/>
                    <xsl:text disable-output-escaping="yes">]]&gt;</xsl:text>
                </description>
                <image>
                    <title>
                        <xsl:value-of select="$channel-title"/>
                    </title>
                    <link>
                        <xsl:value-of select="$channel-link"/>
                    </link>
                    <url>
                        <xsl:value-of select="$channel-image"/>
                    </url>
                </image>
                <xsl:apply-templates select="//div[contains(@class, 'fbUserStory') or contains(@class, 'userContentWrapper')]"/>
            </channel>
        </rss>
    </xsl:template>
</xsl:stylesheet>
