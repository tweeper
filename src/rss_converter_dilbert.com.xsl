<!--
  Stylesheet to convert Dilbert daily strips to RSS.

  Copyright (C) 2013-2018  Antonio Ospite <ao2@ao2.it>

  This file is part of tweeper.

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
-->

<!--
  Since June 18, 2013 dilbert.com strips are not accessible anymore
  directly from the RSS feed, this message is displayed instead:

    Dilbert readers - Please visit Dilbert.com to read this feature. Due
    to changes with our feeds, we are now making this RSS feed a link to
    Dilbert.com.

  How unhandy is that, was it because of a management decision?
  Maybe a parody dilbert strip is needed about this issue...
-->

<xsl:stylesheet version="1.0"
    xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:php="http://php.net/xsl"
    exclude-result-prefixes="php">

    <xsl:param name="generate-enclosure"/>
    <xsl:param name="show-multimedia"/>

    <xsl:output method="xml" indent="yes"/>

    <xsl:variable name="BaseURL" select="//meta[@property='og:url']/@content"/>

    <xsl:template match="//section[@class='comic-item']">
        <xsl:variable name="item-permalink" select=".//a[@class='img-comic-link']/@href"/>
        <xsl:variable name="picture-url" select=".//img[@class='img-responsive img-comic']/@src"/>
        <xsl:variable name="picture-title" select=".//img[@class='img-responsive img-comic']/@alt"/>
        <item>
            <title>
                <xsl:variable name="title-length" select="140"/>
                <!-- ellipsize, inspired from http://stackoverflow.com/questions/13622338 -->
                <xsl:choose>
                    <xsl:when test="string-length($picture-title) > $title-length">
                        <xsl:variable name="truncated-length" select="$title-length - 3"/>
                        <xsl:value-of select="substring($picture-title, 1, $truncated-length)"/>
                        <xsl:text>...</xsl:text>
                    </xsl:when>
                    <xsl:otherwise>
                        <xsl:value-of select="$picture-title"/>
                    </xsl:otherwise>
                </xsl:choose>
            </title>
            <link>
                <xsl:value-of select="$item-permalink"/>
            </link>
            <guid>
                <xsl:value-of select="$item-permalink"/>
            </guid>
            <pubDate>
                <xsl:value-of select="php:functionString('Tweeper\Tweeper::strToRssDate', normalize-space(.//date))"/>
            </pubDate>
            <description>
                <xsl:text disable-output-escaping="yes">&lt;![CDATA[</xsl:text>
                <xsl:if test="$show-multimedia = 1">
                    <img src="{$picture-url}" alt="{$picture-title}"/>
                </xsl:if>
                <xsl:text disable-output-escaping="yes">]]&gt;</xsl:text>
            </description>
            <xsl:if test="$generate-enclosure = 1">
                <!--
                    Dilbert.com uses protocol-relative urls for pictures but
                    generateEnclosure() relies on curl which cannot work
                    without a schema.

                    Use http as protocol because curl gives some error when
                    attempting TLS negotiation with the server where
                    Dilbert.com assets are.
                -->
                <xsl:copy-of select="php:functionString('Tweeper\Tweeper::generateEnclosure', concat('http:', $picture-url))"/>
            </xsl:if>
        </item>
    </xsl:template>

    <xsl:template match="/">
        <xsl:variable name="channel-title" select="//meta[@property='og:title']/@content"/>
        <xsl:variable name="channel-link" select="$BaseURL"/>

        <rss version="2.0">
            <xsl:attribute name="xml:base"><xsl:value-of select="$BaseURL" /></xsl:attribute>
            <channel>
                <generator>Tweeper</generator>
                <title>
                    <xsl:value-of select="$channel-title"/>
                </title>
                <link>
                    <xsl:value-of select="$channel-link"/>
                </link>
                <description>
                    <xsl:value-of select="//meta[@property='og:description']/@content"/>
                </description>
                <image>
                    <title>
                        <xsl:value-of select="$channel-title"/>
                    </title>
                    <link>
                        <xsl:value-of select="$channel-link"/>
                    </link>
                    <url>
                        <xsl:value-of select="concat($BaseURL, //img[@alt='Dilbert logo']/@src)"/>
                    </url>
                </image>
                <xsl:apply-templates select="//section[@class='comic-item']"/>
            </channel>
        </rss>
    </xsl:template>
</xsl:stylesheet>
